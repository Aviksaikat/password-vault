import sqlite3
import hashlib
from tkinter import *
# this is not a class
from tkinter import simpledialog
from functools import partial

# Database
with sqlite3.connect("pass.db") as db:
    cursor = db.cursor()

# main Database to store master password
cursor.execute("""
CREATE TABLE IF NOT EXISTS masterpass(
id INTEGER PRIMARY KEY,
password TEXT NOT NULL);
""")

# database to password info with username & url
cursor.execute("""
CREATE TABLE IF NOT EXISTS vault(
id INTEGER PRIMARY KEY,
website TEXT NOT NULL,
username TEXT NOT NULL,
password TEXT NOT NULL);
""")

# colors
# kind of black more of a dark
BACKGROUND_COLOR = "#151817"
WHITE = "#FFFFFF"


# initial window
window = Tk()
window.title("Password Manager")


# popup
def popUp(text):
    answer = simpledialog.askstring("input data", text)
    # print(answer)
    return answer


# hashing
def hashedPass(inp):
    hashing = hashlib.sha3_512(inp)
    return hashing.hexdigest()


def firstScreen():
    window.geometry("250x150")
    window.configure(bg=BACKGROUND_COLOR)

    label1 = Label(window, text="Create master password")
    label1.config(anchor=CENTER)
    label1.pack()

    txt1 = Entry(window, width=20, show='*')
    txt1.pack()
    txt1.focus()

    label2 = Label(window, text="Re-enter master password")
    label2.pack()

    txt2 = Entry(window, width=20, show='*')
    txt2.pack()
    txt2.focus()

    label3 = Label(window, fg="#f00")
    label3.pack()

    def savePass():
        if txt1.get() == txt2.get():
            hashed_pass = hashedPass(txt1.get().encode("utf-8"))
            insert_pass = """INSERT INTO masterpass(password)
                             VALUES(?)"""
            cursor.execute(insert_pass, [hashed_pass])
            db.commit()

            passVault()

        else:
            label3.config(text="Password do not match")

    btn = Button(window, text="save", width=10, command=savePass)
    btn.pack(pady=10)


def logInScreen():
    window.geometry("250x150")
    #background
    window.configure(bg=BACKGROUND_COLOR)

    label1 = Label(window, text="Enter master password", fg=WHITE, bg=BACKGROUND_COLOR)
    label1.config(anchor=CENTER)
    label1.pack()

    txt = Entry(window, width=20, show='*')
    txt.pack()
    txt.focus()

    label2 = Label(window, fg="#f00", bg=BACKGROUND_COLOR)
    label2.pack()

    def masterPass():
        check_hashed_pass = hashedPass(txt.get().encode("utf-8"))
        cursor.execute("SELECT * FROM masterpass WHERE id = 1 AND PASSWORD = ?", [check_hashed_pass])
        # print(check_hashed_pass)
        return cursor.fetchall()

    def checkPass():
        match = masterPass()
        # print(match)
        if match:
            # print("Yo")
            passVault()
        else:
            # * delete the wrong inputted password from the pass field
            txt.delete(0, "end")
            label2.config(text="Wrong password")

    btn = Button(window, text="Unlock", width=10, command=checkPass, fg="green", borderwidth=10)
    btn.pack(pady=10)


def addData():
    # clear the pass vault screen
    for widget in window.winfo_children():
        widget.destroy()
    window.geometry("450x250")

    # insert website, username, password
    def addEntry(url, username, password):
        # txt1 = "Website"
        # txt2 = "Username"
        # txt3 = "Password"

        website = url
        username = username
        password = password

        insert_data = """INSERT INTO vault(website, username, password)
                         VALUES(?, ?, ?)"""

        cursor.execute(insert_data, (url, username, password))
        db.commit()

        # refresh the view after adding entry
        passVault()

    url_var = StringVar()
    name_var = StringVar()
    passw_var = StringVar()

    label1 = Label(window, text="url")
    label1.place(relx=0.5, rely=0.1, anchor="center")

    txt1 = Entry(window, textvariable=url_var, width=30)
    txt1.pack()
    txt1.place(relx=0.5, rely=0.2, anchor="center")
    txt1.focus()

    label2 = Label(window, text="username")
    label2.place(relx=0.5, rely=0.3, anchor="center")

    txt2 = Entry(window, textvariable=name_var, width=30)
    txt2.place(relx=0.5, rely=0.4, anchor="center")

    label3 = Label(window, text="password")
    label3.place(relx=0.5, rely=0.5, anchor="center")

    txt3 = Entry(window, textvariable=passw_var, width=30)
    txt3.place(relx=0.5, rely=0.6, anchor="center")

    label4 = Label(window, fg="#f00", bg=BACKGROUND_COLOR)
    label4.place(relx=0.5, rely=0.7, anchor="center")

    def savePass():
        if name_var.get() and passw_var.get() and url_var.get():
            #print(name_var.get())
            addEntry(url_var.get(), name_var.get(), passw_var.get())

        else:
            label4.config(text="Enter all files!!")

    btn = Button(window, text="save", width=10, command=savePass)
    btn.place(relx=0.5, rely=0.8, anchor="center")

    # Back to pass vault
    btn1 = Button(window, text="Back", command=passVault)
    btn1.place(relx=0.1, rely=0.1, anchor="center")


def passVault():
    # * if pass right remove all previously printed texts
    for widget in window.winfo_children():
        widget.destroy()
    window.geometry("750x350")

    # remove website, username, password
    def deleteEntry(id):
        # `,` to make it a str # my best guess
        cursor.execute("DELETE FROM vault WHERE id = ?", (id,))
        db.commit()

        passVault()

    label = Label(window, text="Password Vault", fg="#FFFFFF", bg=BACKGROUND_COLOR)
    label.grid(column=1, pady=10, padx=80)

    # adding password info & all
    btn1 = Button(window, text="Add", command=addData)
    btn1.grid(column=3, row=0)

    lbl = Label(window, text="Website")
    lbl.grid(row=2, column=0, padx=80)
    lbl = Label(window, text="Username")
    lbl.grid(row=2, column=1, padx=80)
    lbl = Label(window, text="Password")
    lbl.grid(row=2, column=2, padx=80)

    # the next 2 line should be together
    cursor.execute("SELECT * FROM vault")
    if (cursor.fetchall() != None):
        i = 0
        while True:
            cursor.execute("SELECT * FROM vault")
            array = cursor.fetchall()

            lbl1 = Label(window, text=(array[i][1]), font=("Helvetica", 12))
            lbl1.grid(column=0, row=(i + 3))
            lbl2 = Label(window, text=(array[i][2]), font=("Helvetica", 12))
            lbl2.grid(column=1, row=(i + 3))
            lbl3 = Label(window, text=(array[i][3]), font=("Helvetica", 12))
            lbl3.grid(column=2, row=(i + 3))

            btn = Button(window, text="Delete", command=partial(deleteEntry, array[i][0]))
            btn.grid(column=3, row=(i + 3), pady=10)

            i += 1

            # check for remaining items
            cursor.execute("SELECT * FROM vault")
            if (len(cursor.fetchall()) <= i):
                break


cursor.execute("SELECT * FROM masterpass")

if cursor.fetchall():
    logInScreen()
else:
    firstScreen()

window.mainloop()
